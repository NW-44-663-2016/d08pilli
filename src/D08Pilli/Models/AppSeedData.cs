﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08Pilli.Models
{
    public static class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context == null) { return; }
            if (context.UpcomingEvents.Any()) { return; }



            Location location1 = new Location() { Latitude = 25.7616798, Longitude = -80.1917902, Place = "Miami", State = "Florida", Country = "USA" };
            Location location2 = new Location() { Latitude = 30.245115, Longitude = -97.767372, Place = "Albuquerque", State = "New Mexico", Country = "USA" };
            Location location3 = new Location() { Latitude = 35.482263, Longitude = -86.088642, Place = "Austin", State = "Texas", Country = "USA" };
            Location location4 = new Location() { Latitude = 35.482263, Longitude = -86.088642, Place = "Manchester", State = "Tennessee", Country = "USA" };
            Location location5 = new Location() { Latitude = 40.691018, Longitude = -74.046475, Place = "New York City", State = "New York", Country = "USA" };
            Location location6 = new Location() { Latitude = 47.6062095, Longitude = -122.3320708, Place = "Seattle", State = "Washington", Country = "USA" };
            Location location7 = new Location() { Latitude = 32.713301, Longitude = -117.17001, Place = "San Diego", State = "California", Country = "USA" };

            var locationLst = new List<Location>()
            {
                location1, location2, location3, location4, location5, location6, location7
            };
            context.Locations.AddRange(locationLst);

            var upcomingEventsLst = new List<UpcomingEvent>()
            {
               new UpcomingEvent() { eventName = "Art basel Miami", eventType = "Art", eventDate = DateTime.Parse("12/01/2016"), eventLocation = "Miami", eventDuration = 4.50 },
               new UpcomingEvent() { eventName = "Albuqueruq International Balloon Fiesta", eventType = "festival", eventDate = DateTime.Parse("10/01/2016"), eventLocation = "Albuquerque", eventDuration = 2.50 },
               new UpcomingEvent() { eventName = "Austin City Limits Festival", eventType = "festival", eventDate = DateTime.Parse("09/30/2016"), eventLocation = "Austin", eventDuration = 2.00 },
               new UpcomingEvent() { eventName = "Bonnaroo", eventType = "festival", eventDate = DateTime.Parse("06/09/2016"), eventLocation = "Manchester", eventDuration = 3.00 },
               new UpcomingEvent() { eventName = "Brooklyn Hip-Hop Festival", eventType = "Festival", eventDate = DateTime.Parse("07/13/2016"), eventLocation = "New York City", eventDuration = 6.00 },
               new UpcomingEvent() { eventName = "Bumbershoot", eventType = "festival", eventDate = DateTime.Parse("09/04/2016"), eventLocation = "Seattle", eventDuration = 2.00 },
               new UpcomingEvent() { eventName = "Comic-Con International", eventType = "festival", eventDate = DateTime.Parse("07/21/2016"), eventLocation = "San Diego", eventDuration = 4.00 }
            };


            context.UpcomingEvents.AddRange(upcomingEventsLst);

            
            context.SaveChanges();
        }
    }
}
