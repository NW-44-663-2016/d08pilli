﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace D08Pilli.Models
{
    public class UpcomingEvent
    {
        [ScaffoldColumn(false)]
        [Key]
        public int UpcomingEventID { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        public String eventName { get; set; }

        [Display(Name = "Event Type")]
        public string eventType { get; set; }

        [RegularExpression(@"/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/", ErrorMessage = "Invalid date")]
        [Display(Name = "date")]
        public DateTime eventDate { get; set; }

        [Display(Name = "Location")]
        public string eventLocation { get; set; }

        [Range(typeof(double), "0.50", "24.00")]
        [Display(Name = "Duration")]
        public double eventDuration { get; set; }

       
    }
}
