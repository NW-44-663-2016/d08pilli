using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Pilli.Models;

namespace D08Pilli.Controllers
{
    [Produces("application/json")]
    [Route("api/UpcomingEvents")]
    public class UpcomingEventsController : Controller
    {
        private ApplicationDbContext _context;

        public UpcomingEventsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/UpcomingEvents
        [HttpGet]
        public IEnumerable<UpcomingEvent> GetUpcomingEvents()
        {
            return _context.UpcomingEvents;
        }

        // GET: api/UpcomingEvents/5
        [HttpGet("{id}", Name = "GetUpcomingEvent")]
        public IActionResult GetUpcomingEvent([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            UpcomingEvent upcomingEvent = _context.UpcomingEvents.Single(m => m.UpcomingEventID == id);

            if (upcomingEvent == null)
            {
                return HttpNotFound();
            }

            return Ok(upcomingEvent);
        }

        // PUT: api/UpcomingEvents/5
        [HttpPut("{id}")]
        public IActionResult PutUpcomingEvent(int id, [FromBody] UpcomingEvent upcomingEvent)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != upcomingEvent.UpcomingEventID)
            {
                return HttpBadRequest();
            }

            _context.Entry(upcomingEvent).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UpcomingEventExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/UpcomingEvents
        [HttpPost]
        public IActionResult PostUpcomingEvent([FromBody] UpcomingEvent upcomingEvent)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.UpcomingEvents.Add(upcomingEvent);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UpcomingEventExists(upcomingEvent.UpcomingEventID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetUpcomingEvent", new { id = upcomingEvent.UpcomingEventID }, upcomingEvent);
        }

        // DELETE: api/UpcomingEvents/5
        [HttpDelete("{id}")]
        public IActionResult DeleteUpcomingEvent(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            UpcomingEvent upcomingEvent = _context.UpcomingEvents.Single(m => m.UpcomingEventID == id);
            if (upcomingEvent == null)
            {
                return HttpNotFound();
            }

            _context.UpcomingEvents.Remove(upcomingEvent);
            _context.SaveChanges();

            return Ok(upcomingEvent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UpcomingEventExists(int id)
        {
            return _context.UpcomingEvents.Count(e => e.UpcomingEventID == id) > 0;
        }
    }
}